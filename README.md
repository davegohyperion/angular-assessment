# Angular Assessment

Repo for Hyperion's Angular Assessment

## Requirements

Use the [simple-keyboard](https://github.com/hodgef/simple-keyboard) repo for your numeric keypad

Assessment should take between 1-3 hours.
#### Please do NOT spend more than 3 hours on this assessment.

## Objective

The idea is to use a numeric keypad that expects three unique 4-digit pins. Once three unique pins have been entered, cross-reference the entered pins with 3 predetermined pin values of your choosing and verify they match. If the three pins are a match, route to another component which shows a success message.

## Implementation 

* Use smart/dumb component architecture.
* Embed keypad component into home component. Create a service that is used to send keypad input and get keypad input as an observable.
* Compare two arrays and verify the values between them are identical.
* Lazy load the success component.

## Completed Instructions

Once you've completed the project do the following
* Create a branch with your name
* Stage and commit your changes
* Publish your newly created branch to the remote repo